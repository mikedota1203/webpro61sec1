<?php

use Illuminate\Database\Seeder;
//use Illuminate\Foundatio\Auth;

class UserTableSeeder extends Seeder
{


    public function run()
    {
        DB::table('users')->delete();
        Illuminate\Foundation\Auth\User::create(array(
            'name'     => 'phatthanaphong',
            'username' => 'joe',
            'email'    => 'joe@smsu.ac.th',
            'password' => Hash::make('awesome'),
        ));
    }

}
