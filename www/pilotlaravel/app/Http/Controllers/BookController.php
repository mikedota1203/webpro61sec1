<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /*function store(Request $request){
        $book = new Books();
        $book->setAttibute("name",$request->name);
        $book->setAttibute("category",$request->category);
        $book->setAttibute("description",$request->description);
        if($book->save()){
            return true;
        }
    }*/
    //เพิ่ม
    function store(Request $request){
        $book = new Books();
        if(Books::created($request->all())){
         return true;
        }
    }
    //แก้ไข 3
    function update(Request $request, Books $book){
        if($book->fill($request->all())->save()){
            return true;
        }
    }
    //killy 4
    function index(){
        $books = Books::all();
        return $books;
    }
    //show 5
    function show(Books $books){
        return $books;
    }
    //delete 6
    function  destroy(Books $books){
        if($books->delete()){
            return true;
        }
    }
}
